import './App.css';
import search from './assets/search.svg';
import React, {useState} from 'react';
import axios from 'axios';

function App() {

  const [codigo, setCodigo] = useState('');
  const [ncm, setNcm] = useState('');
  const [data, setData] = useState([]);

  const getNcm = () => {
    axios.get(`http://18.232.94.243:3000/ncm/${codigo}`, { crossDomain: true })
        .then(response => {
            setData(response.data);
    });
}

  const pesquisarCodigo = () => {
    if (codigo.length <= 7 || codigo.length >8) {
      alert("Código NCM inválido!");
      setCodigo('');
      return;
    }
    getNcm();
    setNcm(`NCM ${codigo}: ${data.descricao}`);
    setCodigo('');
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={search} alt="search logo" />
        <p>
          Consulte informações atualizadas do NCM
        </p>
        <div>
          <input type='text' value={codigo} onChange={e => setCodigo(e.target.value)} placeholder="Digite o código desejado"></input>
          <button onClick={pesquisarCodigo}>Pesquisar</button>
        </div>
        <div>
          <p>{ncm}</p>
        </div>
      </header>
    </div>
  );
}

export default App;
